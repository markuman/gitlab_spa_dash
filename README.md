# Gitlab SPA Dashboard

displays latest pipeline status of a various number of projects


1. create a personal API Access Token (settings )
2. visit https://markuman.gitlab.io/gitlab_spa_dash and enter your details


Or pass your settings as attribute to the url

https://markuman/gitlab.io/gitlab_spa_dash/index.html?access_token=YOURACCESSTOKEN&projects=firefox-headless,gitlab_spa_dash,nginx-vts&gitlab_host=gitlab.com

# Limitations

* It will only fetch projects where you are a member of
* It will max fetch 100 Projects where you are a member of
* Refresh rate is 60 seconds
* display colors for `success` (green), `failed` (red), `running` (blue) and `pending` (orange)





# Desktop

![desktop](https://gitlab.com/markuman/gitlab_spa_dash/uploads/519eb1f6d1677553ac1673e34053f2ee/desktop.png)



# Mobile



![mobile1](https://gitlab.com/markuman/gitlab_spa_dash/uploads/130f858b3429fc67fd7bf1fb623f9cb3/Screenshot_20180321-145136.png)

![mobile2](https://gitlab.com/markuman/gitlab_spa_dash/uploads/0e56e99ce468a7e87c1b768b011a4b1f/Screenshot_20180321-145125.png)
