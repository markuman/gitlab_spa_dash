export default {
  template: `
  <div id="app" class="container">
    <span class="itemtitle">Your Gitlab SPA Dash Projects</span><br />
    <div class="underline"></div>

    <div v-if="init === 'yes'" class="cols">
      <h3>Please insert a access_token</h3><br />
      <p>you can also request for https://markuman/gitlab.io/gitlab_spa_dash/index.html?access_token=YOURACCESSTOKEN&projects=firefox-headless,gitlab_spa_dash,nginx-vts</p><br />
      <input v-model="access_token" v-on:keyup.enter="init_projects" placeholder="gitlab access token"><br />
      <h3>insert comma separated project list</h3>
      <input v-model="project_input" v-on:keyup.enter="init_projects" placeholder="gitlab_spa_dash,nginx-vts,firefox-headless"><br />
      <h3>insert gitlab host</h3>
      <input v-model="gh" v-on:keyup.enter="init_projects"><br />
    </div>


    <div v-else-if="projects.length == 0" class="cols">
      <h3>please wait ...</h3><br />
      <div class="loader"></div>
    </div>

    <div v-else-if="data.length > 0" class="items">
      <div class="item" v-for="project in data" :key="project.id">
        <table>
            <tr>
                <th v-bind:class="project.status" colspan="2"><a v-bind:href="project.url">{{project.name}}</a></th>
            </tr>
            <tr>
                <td>Branch</td>
                <td>{{project.branch}}</td>
            </tr>
            <tr>
                <td>Status</td>
                <td>{{project.status}}</td>
            </tr>
        </table>
      </div>
    </div>
  </div>
  `,
data() {
    return {
      init: 'yes',
      access_token: '',
      whitle_list: [],
      projects: [],
      menu: 'home',
      data: [],
      project_input: '',
      gh: 'gitlab.com'
    }
  },
  methods: {
    is_project_in_whitelist(name) {
      for (let idx in this.whitle_list) {
        if (name.indexOf(this.whitle_list[idx]) > -1) {
          return true
        }
      }
      return false
    },
    single_update(projectid, path, name) {
      fetch(`${this.base_url}/projects/${projectid}/pipelines?private_token=${this.access_token}`)
        .then(res => res.json())
        .then(data => {
            this.update_id(name, data[0].status, `https://${this.gitlab_host}/${path}/pipelines/${data[0].id}`)
        })
    },
    update_id(name, status, url){
      for (let idx in this.data){
        if (this.data[idx].name === name) {
          this.data[idx].status = status
          this.data[idx].url = url
          break
        }
      }
    },
    interval_update() {
        if (this.projects.length > 0){
          for (let idx in this.projects){
            this.single_update(this.projects[idx].id, this.projects[idx].path, this.projects[idx].name)
          }
        } else {console.log('projects still empty')}
    },
    init_data(id, path, name) {
      fetch(`${this.base_url}/projects/${id}/pipelines?private_token=${this.access_token}`)
        .then(res => res.json())
        .then(data => {
            if (data.length > 0) {
                this.data.push(
                    {branch: data[0].ref, 
                    status: data[0].status,
                    id: data[0].id,
                    name: name,
                    url: `https://${this.gitlab_host}/${path}/pipelines/${data[0].id}`})
            }
        })
    },
    update_projects(page){
      fetch(`${this.base_url}/projects?membership=1&per_page=10&page=${page}&private_token=${this.access_token}`)
        .then(res => res.json())
        .then(data => {
          for (let idx in data){
            if (this.is_project_in_whitelist(data[idx].name)) {
              let obj = {}
              obj.id = data[idx].id
              obj.name = data[idx].name
              obj.path = data[idx].path_with_namespace;
              this.projects.push(obj)
              this.init_data(obj.id, obj.path, obj.name)
            }
          }
      })
    },
    init_projects(){
      if (this.project_input.length > 0) {
        this.whitle_list = this.project_input.split(',');
      }
      this.init = 'no'
      for (let page = 1; page < 10; page++) {
        this.update_projects(page)
      }
    }

  },
  computed: {
    base_url: {
      get: function() {
       return 'https://' + this.gitlab_host + '/api/v4' 
      }
    },
    gitlab_host: {
      get: function() {
        return this.gh
      }
    }
  },
  mounted() {
    setInterval(function () {
      this.interval_update();
    }.bind(this), 15000); 
  },
  beforeMount() {
    let url = new URL(document.URL);
    let at = url.searchParams.get('access_token');
    let wl = url.searchParams.get('projects');
    let gh = url.searchParams.get('gitlab_host');

    if (typeof wl === 'string') {
      this.whitle_list = wl.split(',');
    }
    
    if (typeof gh === 'string') {
      this.gh = gh;
    }

    if (typeof at === 'string') {
      this.access_token = at;
      this.init_projects()
    }
  }
}
